﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {

        #region Zmienne
        static int ilosc_pytan = 30;
        int scoree = 0;
        string[] answer = new string[4]; //-> ODPOWIEDZI
        string[] questions = new string[ilosc_pytan + 2]; // pytania
        string question; //pytanie po edycji
        int wylosowany_nr_pytania;
        int i = ilosc_pytan;
        bool tak_nie;
        string correct_answer;
        int los_odp;
        bool yes;

        static int[] odp = new int[5];
        static int[] wylosowane = new int[ilosc_pytan];
        #endregion

        static int Losowanie(int zakres)
        {
            Random random = new Random();
            return random.Next(0, zakres);
        }

        static bool CzyByloWylosowane(int nr)
        {
            for (int i = 0; i < ilosc_pytan; i++)
            {
                if (wylosowane[nr] == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public void Prawidlowa_odp(Button button, string t)
        {
            if (button.Text == t)
            {
                scoree++;
                //label1.Text = "Punkty: " + scoree;
                Pyt();
            }
            else Pyt();
        }

        static bool CzyBylaOdp(int nr)
        {
            for (int i = 0; i < 4; i++)
            {
                if(odp[nr] == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Pyt()
        {
            if(i == 0)
            {
                string str = string.Format("Zdobyłeś {0} punktów.\nOsiągnąłeś {1}%.", scoree, (scoree * 100) / ilosc_pytan);
                if (MessageBox.Show(str, "Fiodor Dostojewski - \"Zbrodnia i kara\" by Damian Antoniak", MessageBoxButtons.OK, MessageBoxIcon.Question) == DialogResult.OK)
                //if (MessageBox.Show(str, "Fiodor Dostojewski - \"Zbrodnia i kara\" by Damian Antoniak", MessageBoxButtons.OK, MessageBoxIcon.Exclamation) == DialogResult.OK) ;
                {
                    Application.Exit();
                    return true;
                }
            }

            i--;

            do
            {
                wylosowany_nr_pytania = Losowanie(ilosc_pytan); //Zaczynam Losowanie pytania
                tak_nie = CzyByloWylosowane(wylosowany_nr_pytania); //Sprawdzam czy pytanie bylo juz wylosowane
            }
            while (tak_nie == true);

            wylosowane[wylosowany_nr_pytania] = 1;

            int find = questions[wylosowany_nr_pytania].IndexOf(";");
            
            question = questions[wylosowany_nr_pytania].Substring(0, find);
            button5.Text = question;//Tresc pytania

            questions[wylosowany_nr_pytania] = questions[wylosowany_nr_pytania].Remove(0, find + 1);
            answer = questions[wylosowany_nr_pytania].Split(new char[] { ':' }, 4);

            correct_answer = answer[3];
            #region OLD
            //
            /*button1.Text = answer[0];
            button2.Text = answer[1];
            button3.Text = answer[2];
            button4.Text = answer[3]; //Prawidlowa*/
            #endregion

            for (int k = 0; k < 4; k++) odp[k] = 0;

            #region Mieszanie_odpowiedzi
            for (int j = 0; j < 4; j++)
            {
                do
                {
                    los_odp = Losowanie(4);
                    yes = CzyBylaOdp(los_odp);
                }
                while (yes == true);

                odp[los_odp] = 1;

                switch(j)
                {
                    case 0:
                        {
                            button1.Text = answer[los_odp];
                            break;
                        }

                    case 1:
                        {
                            button2.Text = answer[los_odp];
                            break;
                        }

                    case 2:
                        {
                            button3.Text = answer[los_odp];
                            break;
                        }

                    case 3:
                        {
                            button4.Text = answer[los_odp];
                            break;
                        }
                }
            }
            #endregion

            return false;
        }

       

        public Form2()
        {
            InitializeComponent();
            #region Old
            /*string[] question = new string[2]; // hide?
            question[1] = "Kiedy urodził się Fiodor Dostojewski?";
            richTextBox1.Text = question[1];
            button5.Text = question[1];
             
            string[] answer = { "1821", "1850", "1760", "1943" };// hide?
            button1.Text = answer[0];
            button2.Text = answer[1];
            button3.Text = answer[2];
            button4.Text = answer[3];*/
            #endregion

            int counter = 0;

            #region Old
            /*string[] questions = new string[8];
            int wylosowany_nr_pytania;
            bool tak_nie;*/
            #endregion

            System.IO.StreamReader file = new System.IO.StreamReader(@"pl/Sys.wax", Encoding.Default);
            while ((questions[counter] = file.ReadLine()) != null) counter++;
            file.Close();

            Pyt();

            #region Edit
            /*for (int i = 0; i < 6; i++)
            {
                do
                {
                    wylosowany_nr_pytania = Losowanie(6);//Zaczynam Losowanie pytania
                    tak_nie = CzyByloWylosowane(wylosowany_nr_pytania);//Sprawdzam czy pytanie bylo juz wylosowane
                }
                while (tak_nie == true);

                wylosowane[wylosowany_nr_pytania] = 1;
                int find = questions[wylosowany_nr_pytania].IndexOf(";");
                //string question = questions[wylosowany_nr_pytania].Substring(0, find);
                question = questions[wylosowany_nr_pytania].Substring(0, find);
                button5.Text = question;//Tresc pytania

                questions[wylosowany_nr_pytania] = questions[wylosowany_nr_pytania].Remove(0, find + 1);

                //string[] answer = questions[wylosowany_nr_pytania].Split(new char[] { ':' }, 4);
                answer = questions[wylosowany_nr_pytania].Split(new char[] { ':' }, 4);

                //Odpowiedzi
                button1.Text = answer[0];
                button2.Text = answer[1];
                button3.Text = answer[2];
                button4.Text = answer[3]; //Prawidlowa
            }*/
            #endregion
        }

        private void label1_Click(object sender, EventArgs e)//Punkty
        {

        }

        private void allButton_Click(object sender, EventArgs e)//ODP_D
        {
            Prawidlowa_odp((sender as Button), answer[3]);
        }

        #region Hover & Leave

        private void allButton_MouseHover(object sender, EventArgs e)
        {
            (sender as Button).ForeColor = System.Drawing.ColorTranslator.FromHtml("#0d1b1f");
        }

        private void allButton_MouseLeave(object sender, EventArgs e)
        {
            (sender as Button).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(75)))), ((int)(((byte)(86)))));
        }

        #endregion
    }
}
