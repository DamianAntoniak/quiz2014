﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private Form2 okno = null; //referencja na Twoje okno;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            button1.Text = "mleko";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(okno != null) return; //sprawdzasz czy okno jest, jeżeli nie, to tworzysz

             okno = new Form2();
             okno.Show();

             this.Hide();
             //tutaj dodajesz event żeby ustawiał referencje na null, w momencie zamknięcia okna.
             okno.FormClosed += new FormClosedEventHandler(okno_FormClosed);
        }

        void okno_FormClosed(object sender, FormClosedEventArgs e)
        {
            okno = null;
            Application.Exit();
            //this.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void allButton_MouseHover(object sender, EventArgs e)
        {
            (sender as Button).ForeColor = System.Drawing.ColorTranslator.FromHtml("#0d1b1f");
        }

        private void allButton_MouseLeave(object sender, EventArgs e)
        {
            (sender as Button).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(75)))), ((int)(((byte)(86)))));
        }
    }
}
